# @react-native-ohos/react-native-splash-screen
This project is based on  [react-native-splash-screen](https://github.com/crazycodeboy/react-native-splash-screen)
## Documentation
[中文](https://gitee.com/react-native-oh-library/usage-docs/blob/master/zh-cn/react-native-splash-screen.md)

[English](https://gitee.com/react-native-oh-library/usage-docs/blob/master/en/react-native-splash-screen.md)

## License
This library is licensed under [The MIT License (MIT)](https://gitee.com/openharmony-sig/rntpc_react-native-splash-screen/blob/master/LICENSE).
