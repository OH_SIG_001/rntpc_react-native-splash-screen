 // Copyright (c) 2016 Jia PengHui. All rights reserved.
 // Use of this source code is governed by a MIT license that can be
 // found in the LICENSE file.

/**
 * SplashScreen
 * 启动屏
 * from：http://www.devio.org
 * Author:CrazyCodeBoy
 * GitHub:https://github.com/crazycodeboy
 * Email:crazycodeboy@gmail.com
 * @flow
 */
'use strict';

import NativeModules from './src/RNSplashScreenModule';
export default NativeModules;

